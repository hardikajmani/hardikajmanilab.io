---
title: "The subtle art of Giving"
date: 2019-10-27T12:08:05+05:30
draft: false
author: Hardik Ajmani
image: images/blog/giving.jpg
description : "The subtle art of Giving."
summary : "A perfect way to begin your Diwali with."
---
<center><b><i>”We make a living by what we get, but we make a life by what we give.” Winston Churchill</i></b></center>

<p> It's been a while that I have shared my thoughts, and I believe it's the most subtle topic to be conversed about on this prosperous day.
    As we are advancing in the technological era, there is logically no need for exchange of emotions. With all the fantastic mood converting content, artificial intelligence-powered mood changers ectara ectara. Resulting in an exponential increase in the urge of withdrawal. It's been long that we are only concerned about <b>"taking"</b> rather than <b>"giving"</b>. 
</p>
<p>
    You want attention even if you haven't given, you want luxuries even if you haven't hustled, you want to fulfil your physical desires even if you don't deserve, you want entertainment from those polluting crackers even if you read about global warming every day, you want respect even if you haven't given. The madness of want is so much that we cannot move an inch until and unless we are guaranteed something in return.
    <br>
    Here, I will try to rejuvenate the need of unconditional giving, majorly in relationships, <i>kachra mat pheko, ped lagao apko bhi pata h</i>. It is because lately, peace of my mind has been seriously rattled by the pain and suffering in the eyes of people because of unhappy relationships. Not just bloody couples, but with fundamental relations like parents.
</p>
<ul>
    <li> <b>Givers are returned (<i>Dene se puniya milta h</i>)</b>
            <p>You might have heard from your elders and priests <i>"aaj daan kardo shanivar h"
            </i> or <i>"kutte ko parleG ki biscut khila do shadi ho jaegi"</i>. Well definitely not these, but I believe the satisfaction after giving is intensely incredible than the pleasure of taking. Practice a straightforward habit a day can ensure a fantastic day, which in turn charges you to hustle more and is rewarded later definitely in the form of success.
            </p>
    </li>
    <li> <b>Its anything, but dealing</b>
        <p>In very similar nature to mentioned above, most of the gentlemen and gentleladies do anything so that they are returned with equal or favourably more benefits. To my regret, people treat their relationships like drug deals and then expect a joyous bond. <i>"Maine uske lie itna kia, usne badle me kya dia?"</i> Believe me, if you are doing something with the intentions to be awarded, then sorry, my friend, there is a significant issue in your mindset. You should not care about people outside your circle of influence, and you should not expect returns at least from people as dear to inside your circle of influence.</p>
    </li>
</ul>
<p>
    I think I have hammered enough to make you understand the importance of giving, but how and where to start with, the following might suffice.
</p>


<ul>
    <li>
        <b>Genuine praise</b>
        <p>
            Learn to praise someone unconditionally. Although there is a small line between flattery and praise <i>(makhan and tareef)</i>, so understand and learn the skill. Sometimes an insignificant comment like <i>"smile badi achi h tumahri, ya aaj ache lag rahe ho"</i> from loved ones, can engrave an everlasting impression.
        </p>
    </li>
    <li>
        <b>Some seconds</b>
        <p>
                I belong to a fast city like Mumbai, where time is the primary cause of the demolition of the relationships. Take a moment to figure out whether that episode or party matters you more than the lady or man of your heart, whether the unnecessary feed of insta is more critical than a conversation with your mom. <b>Best things take time, and if you want peace and an unbreakable bond, this point is your gamble.</b>
        </p>
    </li>
    <li>
        <b>Empathy, not sympathy</b>
        <p>
                Empathy or compassion is to understand the situation of the opposite person without filters of your personal experiences. This is one of the most powerful and underused tools to strengthen bonds. Yes, one can relate to his/her past experiences to understand better, but crying about your own experience definitely won't help. There is a constant urge in human beings to prove that they are the most suffering living creature on this planet. Imagine your mom sharing her experience about bank fraud, and we reply like <i>"apko pata h, mujhe bhi autowale ne 20 ka chutta vapis nahi kia"</i>. I hope you get my point, how not to empathize.
        </p>
    </li>
    <li>
        <b>Doing nothing</b>
        <p>
                This is indeed a hidden tool, that is again not used very frequently. Innumerable times people jsut expects you to be a listener as a co-partner in your relationships. Nevertheless, to mention, listening without compassion is equal to listening to a biology lecture. <br>
                Yet, if you are listening, so that she/he should listen to your part, then again you are in a giant trap. Give this a try, empathetically listen to your best friend, boyfriend or mother. You will learn new perspectives, understand the actual situation better, and also give them the solutions that they already had hidden in their minds.
        </p>
    </li>
    <li>
        <b>Grudges</b>
        <p>
                This one is hard, and yes, it takes efforts. Try to solve your year-old grudges with people, with whom you had sworn never to talk. I recently did this with a fantastic old friend of mine, and believe me, it feels incredible. It would not be easy to accept or re-live your mistakes or those not so fabulous moments. Still, these are better to be faced and erased rather than kept as a rioting old memory.
        </p>
    </li>
    <li>
        <b>Show-off your 32</b>
        <p>
                This point is especially for all the people with whom you interact on a day-to-day basis. Go on pass a beatific smile to the teacher, or manager, or colleague or classmate, or security guard <i>(ladies beware, aram se karna, varna ye lattu na ho jae XD)</i>, your parents, or your flatmates. Go on smile and exchange greetings. <br>
                There are endless benefits to you obviously, and a colossal possibility of making the day of the opposite person with your smile. I have been trying to follow this recently, and yes, it does has a positive effect on you and is definitely relationship manure. But do not expect a response if your travelling with a frown on your face recently XD.     
        </p>
    </li>
</ul>
    
    
     
 <p>
        I really feel that the religion of sharing unconditional love is missing these days from society. An initiative towards this remarkable skill on this auspicious day is more beneficial to you than any materialistic offerings to God.
        <br>
        <br>
        Happy Diwali <span style="color:red">&#10084;</span>
        <br>
        Peace
 </p>
    
 <hr>
<p>
        PS - This topic ain't this short, there are whole books written on this. I earnestly urge to read those.
        <br>
        PPS - Diwali is always been celebrated for practicing the correct against all the odds. Take this day as an initiative to do the correct irrespective of how it feels. <i>Ek thank you bhi boldoge kam ho jaega</i>.
        
</p> 
     
    
    
    