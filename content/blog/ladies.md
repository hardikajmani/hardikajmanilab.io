---
title: "Ladies, this is why we stop you."
date: 2019-08-16T12:49:49+05:30
draft: false
author: Hardik Ajmani
image: images/blog/ladies.jpg
description : "Ladies, this is why we stop you."
summary : "The un-understood concern."
---

<center><b><i>"There is a reason Lakshmi is Goddess of Wealth, women, you not only attract material wealth, but you are also our actual wealth."</i></b></center>

<p>Ladies, innumerable times you all get into constant arguments with mother, brother, father, boyfriend, or husband regarding your outfits. <i>(Itne chote kapde phen ke kaunsi baraat me Chali ho?)</i><br>
I think we all can agree that we all have faced or were part of this situation multiple times. And, there is 
no reason to be worried or be in absolute grief about it. <i>(Har ghar ki yahi kahani h!)</i>
<br>
So, on the occasion of Raksha Bandhan, it is an excellent opportunity 
for us to develop a foundation of understanding to avoid or give some efforts to conclude such perpetual disputes.
    
</p>


<ul>
    <li>
        <b>Make you understand the difference</b>
        <p>
                Today, exploitation of buzzwords like feminism or freedom ain't uncommon. 
                Unfortunately, many ladies DO misuse these words to get their work done, without giving a slight 
                notice to the inerasable impact they are creating on the society.
                <br>
                Few days ago, I was in a conversation 
                with two of my fabulous female friends about the type of pictures (un-necessary revealing), models post on insta to gain some likes.
                <i>(And man lo, tumne bhi dekhi hogi aise pictures.)</i> It is quite common, unfortunately.<br>
                So, any negative comments about your outfit, intend wearing something soberer won't decrease your value. 
                And, probably is inappropriate for the occasion.<br>
                Anyone, male or female, needs right clothing worn with adequate confidence and apt 
                attitude to create a significant impact.
        </p>
    </li>

<li>
    <b>Retards exist</b>
    <p>
            This is the grounds of the whole write-up. There is a segment of society which
                is defied of some essential values. 
                <i>(Ye apko comments section me mil jaenge, aur kabhi-kabhi ye log coat bhi pehente
                h)</i>. But, there is no point in blaming them, until some fundamental values are revived or taught to them.
                <br>And until our society is 
                filled with such disabled, your loved ones would and should keep reminding you
                of precautions.
    </p>
</li>

<p>
    Let's be honest, no one likes someone nagging on your head, even if it's for a reason.
    <i>(Mummy, harbari thodi na -_-)</i><br>  Actually, I would also oppose the same. 
    Solution? Keep reading.
 
</p>

<li>
    <b>Location</b>
    <p>
             Location or occasion is a prime factor in deciding your clothing. Even before considering wearing a new dress, 
            and imagining the praises or the Insta picture <i>(Khyali biryani)</i>, give a second to think. 
            What is the location or your means of transport? Can I get along with a more flexible option?<br>
            For example, on a blind date or a meeting with someone, in a lovely café <i>(mahengi jagah)</i>, 
            and traveling via taxi, a one-piece might be a great option.
            But, the worst one, if you are going to a birthday party of your aunt's child and traveling 
            via local train to Nalasopara.<br> 
            I hope you get my point, and most ladies can pull off great outfits on every occasion with a bit of planning. <i>(Kya pehnna h vo app dekhlo, women fashion
            me expert nahi hun.)</i><br>
            This equally applies to men, too, who thinks that fashion is for women only. I can write a whole book on this, lol.
    </p>
</li>

<li>
    <b>Club it with layering</b>
    <p>
            This can work like a charm if creatively applied. One can add a jacket or a 
            scarf to their outfit while traveling to give a more comfortable and manageable look. 
            <i>(But, isme naya kya h?)</i> <br>
            It would give you more independence in choosing the mode of transport. Also, provide satisfaction to your family that yes, you are mature to plan precautions and independent enough to handle yourself well. <br>
            And, it is not very uncommon for ladies to forget such easy hacks these days. 
            <i>(The objective of these blogs is to remind, that its easy. ;)</i>
    </p>
</li>

<li>
    <b>Men, you all suck</b>
    <p>
            We men are all the same. <i>(Hame baat karni nahi aati.)</i> <br>
            No doubt, you have offended your sister or irritated your girlfriend at least once before an exotic outing or a romantic dinner. I have been there, and sometimes we DO mess it up and overdo most of the times. 
            But it's okay, a slight adjustment can help you in expressing the concern, without much of a dispute. I am not explaining the whole process, as it deserves an entire blog.<br> But one hack, I could share is, frame it in the form of a question rather than an order. 
            <i>(Pyar se puchlo, kam easy ho jaega.)</i>
    </p>
</li>

<li>
    <b>Ladies, we love you</b>
    <p>
            There would be times when your parents would order you, or you might feel that he/she is restricting your freedom. 
            But remember, all our anger is just a rough layered concern which is to avoid any 
            unwanted circumstances.<br> And, do not forget, <i>"We LOVE you."</i>
    </p>
</li>

</ul>
Peace.
<br>
<hr>
PS -  I noticed a GAP which needs to be filled with some understanding. I hope this blog helps both the genders equally.<br>
PPS - I have added a like button in here, do give it a push if you liked the write-up.
