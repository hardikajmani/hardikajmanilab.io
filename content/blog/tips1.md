---
title: "Because, Anyone can Click!"
date: 2019-08-10T00:44:38+05:30
draft: false
author: Hardik Ajmani
image: images/blog/tip1.jpeg
description : "Because, Anyone can Click!"
summary : "5 Beginner's tips for photography"
---

<center><b><i>Photography is much more than model shoots or DSLRs.</i></b></center> 
<br/>
It is an art of capturing moments, moments that you want to engrave in a pocket-sized chip. It is an act of capturing a story, so well, that a glance 20 years later is enough to revive those story and bring a smirk on your wrinkled face.
But don't worry, it's not that difficult to start with. 
Follow these tips, and I bet you will see a significant improvement in your pictures. <i>(Chaho to @instapage Bhi khol lena ;)</i>

<ul>
<li>
<b>Don't underestimate the power in your pocket.</b>
<br/>
		Stop blaming the lack of specs or a high-end camera. Gone are those days, when you needed to have a camera to click good quality pictures. Yes, definitely DSLRs can easily beat even best phones in many ways, but believe me, they are great to click some astonishing images.
</li>
<br/>

<li>
    <b>Photographs are made in mind.</b>
    <br/>
This requires practice. Even before you think of opening your camera, think of the frame, subject, angle, etcetera, build an image in mind and then try to replicate in your camera.
Look for symmetry
We human beings are subconsciously and consciously attracted to symmetry. Look for patterns, lines, circles, rectangles anything, imagine, and click.
</li>
<br/>

<li>
    <b>High Dynamic Ranging (HDR)</b>
    <br/>
    This is the most underused ability of a camera. In lame words, as you must have noticed sometimes, there is confusion between focus, and lightings change drastically with focus. So, it takes multiple shots in different lightings and merges them electronically to paint a pretty average of them. Mark my words, it works like a charm for landscapes.<br/> Avoid it in artificial illuminations.
</li>
<br/>

<li>
   <b> There is more to your camera than selfies.</b>
   <br/>
    Most of us, are only aware of two modes in-camera <i>(aage wala aur piche wala XD)</i>, i.e., front and rear camera. But any phone in the market has tones of feature which you even do not know about. So, EXPLORE your camera app, play with features, and eventually, ideas will pop up automatically to be exploited. 
</li>
<br/>

<li>
<b>Get your hands dirty.</b>
<br/>
Practice period.
<br/>
I don't know how to stress more upon this, but practice, find every single opportunity to click a picture. There will be people who have a constant habit of nagging, ignore them.
<i>(Ye dekho photographer, ae teri bhabi ke sath picture le na).</i> Learn to focus on what is necessary and flush out unimportant. You'll continuously learn from your mistakes and hence will see an improvement in yourself.

</li>
<br/>

  



</ul>

<br/>
<hr/>

<b>PS</b> - Ye, 15 min me gym me likha h, koi mistake ho to maaf karna. Grammar weak h tabhi verbal me marks nahi aate ;P
<br/>
<b>PPS</b> - Follow me on <a href = "https://www.instagram.com/aswecapture/">@aswecapture</a> on Instagram, to have a glance at my work.